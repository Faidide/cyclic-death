#include <stdio.h>
#include <string.h>

#include "menu.h"

int main (int argc, char *argv[]) {

  int is_sound = TRUE;

  // test if --no-sound flag is presetn
  if (argc == 2) {
    if (strcmp (argv[1], "--no-sound") == 0) {
      is_sound = FALSE;
    }
  }

  launch_menu (is_sound);
  return 0;
}