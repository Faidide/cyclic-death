#include "game.h"
#include "txt_sprite.h"

#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

// define a macro to handle ctrl plus keys
#define ctrl(x) ((x)&0x1f)

// last refreshed time
struct timeval last_refreshed;
struct timeval last_logical_tick;
struct timeval time_buffer;

int move_left_pressed, move_right_pressed;

TxtSprite rock_sprite, cycle_top_sprite;

int canyon_shift_interval;

// magick number that we set to avoid double call of srand
int srand_magic_number;

// create a GameState object
GameState init_game (int x, int y) {
  // init the timer used to refresh the screen
  gettimeofday (&last_refreshed, NULL);
  gettimeofday (&last_logical_tick, NULL);
  // force a first refresh
  last_refreshed.tv_usec -= REFRESH_TIMEOUT;
  last_logical_tick.tv_usec -= GAME_FREQ;
  // create GameState object
  GameState game;
  // init canyon
  game.canyon_y_offsets = malloc (sizeof (int) * CANYON_LENGTH);
  // in case of problem
  // kill it with fire
  if (game.canyon_y_offsets == NULL) {
    fprintf (stderr, "Fatal error on memory allocation.\n");
    exit (EXIT_FAILURE);
    return game;
  }
  // init all elements with zeros as they are at their intiial pos
  for (int i = 0; i < CANYON_LENGTH; i++) {
    game.canyon_y_offsets[i] = 0;
  }
  // canyon index is at first at 0
  game.canyon_index = 0;
  // compute bike initial position
  game.cyclist_y = (y - 1) / 2;
  game.x_max = 0;
  game.y_max = 0;
  // init lost
  game.lost = 0;
  // compute default canyon y pos
  game.default_canyon_y = (y - CANYON_WIDTH) / 2;
  // return game object copy
  return game;
}

// run one tick of the game, return 1 of lost
int game_logic (GameState *game) {
  // throttle function at 50Hz
  gettimeofday (&time_buffer, NULL);
  if (labs (time_buffer.tv_usec - last_logical_tick.tv_usec) < GAME_FREQ) {
    return 0;
  }

  canyon_shift_interval++;

  if (canyon_shift_interval >= CANYON_SHIFT_INTERVAL) {

    // reset the shift interval iterator
    canyon_shift_interval = 0;

    // shift the canyon
    game->canyon_index = game->canyon_index + 1;
    if (game->canyon_index > 29)
      game->canyon_index = 0;

    // assign new shitfed out cliff part the displacement of the one behind
    // start by computing index of the necessary indexes and make sure they do
    // not overflow
    int previous_cliff_index = game->canyon_index - 2;
    if (previous_cliff_index < 0)
      previous_cliff_index += CANYON_LENGTH;
    int shifted_out_cliff_index = game->canyon_index - 1;
    if (shifted_out_cliff_index < 0) {
      shifted_out_cliff_index += CANYON_LENGTH;
    }
    // make the assignment so that the new shifted out div is aligned with his
    // lower neighbour
    game->canyon_y_offsets[shifted_out_cliff_index] =
        game->canyon_y_offsets[previous_cliff_index];

    // do the random thing to shift it left or right
    int shift = (rand () % 3) - 1;
    game->canyon_y_offsets[shifted_out_cliff_index] =
        game->canyon_y_offsets[shifted_out_cliff_index] + shift;

    // move the bike on key pressed
    if (move_left_pressed == 1) {
      game->cyclist_y = game->cyclist_y - 1;
      move_left_pressed = 0;
    }
    if (move_right_pressed == 1) {
      game->cyclist_y = game->cyclist_y + 1;
      move_right_pressed = 0;
    }

    // test if the player lost
    int left_border =
        game->default_canyon_y - 2 + game->canyon_y_offsets[game->canyon_index];
    int right_border = game->default_canyon_y + CANYON_WIDTH + 1 +
                       game->canyon_y_offsets[game->canyon_index];

    // display debug info
    if (DEBUG == 1) {
      fprintf (game->debug_log,
               "Turn %d:\nPlayer:%d\nLeft border:%d\nRight border:%d\n",
               game->score, game->cyclist_y, left_border, right_border);
    }

    if (game->cyclist_y <= left_border || game->cyclist_y >= right_border) {
      return 1;
    }

    // update score
    game->score = game->score + 1;
  }

  // save the time at which refresh occurs
  gettimeofday (&last_logical_tick, NULL);

  return 0;
}

// draw the object on the screen, return error code
int draw_game (GameState *game) {

  gettimeofday (&time_buffer, NULL);

  if (labs (time_buffer.tv_usec - last_refreshed.tv_usec) < REFRESH_TIMEOUT) {
    return 0;
  }

  // clear the screen
  erase ();

  // get window heigth/width
  int x, y;
  getmaxyx (stdscr, x, y);

  if (x < MIN_WIN_SIZE || y < MIN_WIN_SIZE) {
    // if width is too small, abort and print and error
    fprintf (stderr, "wrong screen size: x:%d y:%d\n", x, y);
    return 1;
  }

  // display cycle
  print_sprite (cycle_top_sprite, x - cycle_top_sprite.heigth,
                game->cyclist_y + 1);

  // display each rock of the cliff
  for (int i = 0; i < CANYON_LENGTH; i++) {
    // compute the index relative to the cliff bottom at canyon_index
    int real_index = i - game->canyon_index;
    if (real_index < 0) {
      real_index += CANYON_LENGTH;
    }
    if (x - real_index < 1) {
      continue;
    }
    print_sprite (rock_sprite, x - real_index - 1,
                  game->default_canyon_y - 2 + game->canyon_y_offsets[i]);
    print_sprite (rock_sprite, x - real_index - 1,
                  game->default_canyon_y + (CANYON_WIDTH) + 2 +
                      game->canyon_y_offsets[i]);
  }

  // display score
  move (x - 1, 0);
  printw ("Score: %d", game->score);

  // moving the ncurses cursor to the end
  move (x - 1, y - 1);
  printw (" ");

  last_refreshed = time_buffer;

  // display what was loaded
  refresh ();

  return 0;
}

// init and play a game
int play_game () {

  // if the magic number is already set ,don't srand
  if (srand_magic_number != 102839279) {
    // intializes random number generator
    srand ((unsigned)time (NULL));
    srand_magic_number = 102839279;
  }

  canyon_shift_interval = 0;

  // get window heigth/width
  int x, y;
  getmaxyx (stdscr, x, y);

  if (x < MIN_WIN_SIZE || y < MIN_WIN_SIZE) {
    // if width is too small, abort and print and error
    printf ("wrong screen size: x:%d y:%d\n", x, y);
    return -1;
  }

  // init game variables
  GameState game = init_game (x, y);

  // create debug log if in debug mode
  if (DEBUG == 1) {
    // opening the file
    game.debug_log = fopen ("debug_log", "ab+");
    if (game.debug_log == (FILE *)NULL) {
      fprintf (stderr, "Unable to open debug log!");
      exit (0);
    }
  }

  game.score = 0;

  // event loop
  // bool to tell main loop to stop
  int must_stop = FALSE;
  int error = 0;

  int score = 0;

  while (must_stop == FALSE) {

    // if there was an error, exit
    if (error != 0) {
      must_stop = TRUE;
      score = -1;
      continue;
    }

    // get char
    int car = getch ();

    // stop if we hit escape or usual exit signals
    if (car == 27 || car == ctrl ('c') || car == ctrl ('d') || car == 'q') {
      // do not register score on abandoned game
      score = 0;
      must_stop = TRUE;
      continue;
    }

    if (car == KEY_LEFT) {
      bike_left (&game);
    }

    if (car == KEY_RIGHT) {
      bike_right (&game);
    }

    int lost = game_logic (&game);

    if (lost == 1) {
      must_stop = TRUE;
      score = game.score;
      continue;
    }

    game.lost = TRUE;

    // draw the elements menu
    error = draw_game (&game);
  }

  free (game.canyon_y_offsets);

  // close debug log if in debug mode
  if (DEBUG == 1) {
    fclose (game.debug_log);
  }

  return score;
}

// // HERE, WE LET GAME LOGIC FUNCTION MOVE THE BIKE
// // WE MERELY REQUEST FOR MOVEMENT
// callback on key pressed
void bike_left (GameState *game) { move_left_pressed = 1; }
void bike_right (GameState *game) { move_right_pressed = 1; }

int load_game_sprites () {
  rock_sprite = load_txt_sprite ("res/ascii/rock.ascii");
  if (rock_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  cycle_top_sprite = load_txt_sprite ("res/ascii/cycle_top.ascii");
  if (cycle_top_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  return 0;
}

// free the allocated sprites
int free_game_sprites () { return 0; }

