#include "scoreboard.h"

#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// file descriptor id for the scoreboard
FILE *score_file;

// read scores from the score file
Scoreboard load_scoreboard () {

  // create scoreboard object
  Scoreboard scoreboard;

  // opening the file
  score_file = fopen ("res/config/scoreboard", "rw+");
  if (score_file == (FILE *)NULL) {
    scoreboard.loading_failed = TRUE;
    return scoreboard;
  }

  // allocate memory
  // sizeof char is useless but hey I like to write it
  scoreboard.names = malloc (sizeof (char) * MAX_NAME_LENGTH * 5);
  scoreboard.scores = malloc (sizeof (int) * 5);

  // handles failure on allocation
  if (scoreboard.names == NULL || scoreboard.scores == NULL) {
    scoreboard.loading_failed = TRUE;
    return scoreboard;
  }

  // read usernames and scores
  for (int i = 0; i < 5; i++) {
    fscanf (score_file, "%s %d\n",
            (char *)(scoreboard.names + (i * MAX_NAME_LENGTH)),
            (scoreboard.scores + (sizeof (int) * i)));
  }

  return scoreboard;
}

// display scoreboard
void display_scoreboard (Scoreboard scoreboard, TxtSprite title) {
  // clear screen
  clear ();

  // get window heigth/width
  int x, y;
  getmaxyx (stdscr, x, y);

  // display scoreboard sprite
  print_sprite (title, 1, (y - title.max_width) / 2);

  // compute coordinates of the sore list corners
  int top_anchor = (title.heigth + (x - title.heigth) / 2) - 5;
  int left_anchor = ((y - SCOREBOARD_WIDTH) / 2);

  // for each score
  for (int i = 0; i < 5; i++) {
    // display player and score
    move (top_anchor + i, left_anchor);
    printw ("%s", (char *)(scoreboard.names + (i * MAX_NAME_LENGTH)));
    move (top_anchor + i, left_anchor + 20);
    printw ("%d", *(scoreboard.scores + (sizeof (int) * i)));
  }

  // I don't like the cursor being in the middle
  move (x - 1, y - 1);

  // refresh screen
  refresh ();

  int quit_scoreboard = FALSE;

  // main loop to hang until player press enter or q
  while (quit_scoreboard == FALSE) {
    // get character pressed
    int car = getch ();

    // exit if q, escape or escape is pressed
    if (car == 'q' || car == 27 || car == 10) {
      quit_scoreboard = TRUE;
    }
  }
}

// add score to scoreboard
void register_score (Scoreboard scoreboard, char *player_name, int score) {
  // variable to store the rank
  int rank = -1;

  // for each score
  for (int i = 0; i < 5; i++) {
    // save rank if our score is higher (older is better)
    int current_score = *(scoreboard.scores + (sizeof (int) * i));
    if (current_score < score) {
      rank = i;
      break;
    }
  }

  // if our score is in the board
  if (rank != -1) {
    // for each following score
    for (int i = 4; i > rank; i--) {
      // move one case down previous scores
      strcpy((char *)(scoreboard.names + (i * MAX_NAME_LENGTH)), (char *)(scoreboard.names + ((i-1) * MAX_NAME_LENGTH)));
      *(scoreboard.scores + (sizeof (int) * i)) = *(scoreboard.scores + (sizeof (int) * (i-1)));
    }
    // insert new score
    strcpy((char *)(scoreboard.names + (rank * MAX_NAME_LENGTH)), player_name);
    *(scoreboard.scores + (sizeof (int) * rank)) = score;
  }

  // close and reopen to start an overwrite
  score_file = fopen("res/config/scoreboard","wb");

  // append the data to the file
  for (int i = 0; i < 5; i++) {
    // save rank if our score is higher (older is better)
    fprintf (score_file, "%s %d\n",
        (char *)(scoreboard.names + (i * MAX_NAME_LENGTH)),
        *(scoreboard.scores + (sizeof (int) * i)));
  }
}

// free the memory allocated
void free_scoreboard (Scoreboard scoreboard) {}