#ifndef CONV_H
#define CONV_H

#define MIN_WIN_SIZE 23

// refresh timeout in microsecond
#define REFRESH_TIMEOUT 5000

// menu elements 
#define DEFAULT_CYCLE_Y 10
#define CYCLE_JITTER 50
#define TREE_MOVE_TIME 3

#include <ncurses.h>
#ifndef NO_SDL
#include <SDL2/SDL.h>
#endif // NO_SDL
void launch_menu ();

int draw_main_menu (WINDOW *win);

// draw the death screen with score when dying
void draw_death_screen (int score);

int load_sprites ();

void save_score (int score);

void display_scoreboard ();

void get_error_message (int error_nb, char **error_msg);

#endif // CONV_H