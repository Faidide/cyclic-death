#ifndef DEF_SCOREBOARD_H
#define DEF_SCOREBOARD_H

#define SCOREBOARD_WIDTH 25

#define MAX_NAME_LENGTH 11

#include "txt_sprite.h"

// scoreboard type
typedef struct Scoreboard {
    char ** names;
    int * scores;
    int loading_failed;
} Scoreboard;

// read scores from the score file
Scoreboard load_scoreboard ();

// display scoreboard
void display_scoreboard (Scoreboard scoreboard, TxtSprite title);

// add score to scoreboard
void register_score (Scoreboard scoreboard, char * player_name, int score);

// free the memory allocated
void free_scoreboard (Scoreboard scoreboard);

#endif // DEF_SCOREBOARD_H