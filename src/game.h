#ifndef GAME_H
#define GAME_H

#define CANYON_LENGTH 30

#define MIN_WIN_SIZE 23

// refresh timeout in microsecond
#define REFRESH_TIMEOUT 5000

#define CANYON_SHIFT_INTERVAL 10

#ifndef FREQUENCY
#define FREQUENCY 50.0
#endif // FREQUENCY

#ifndef CANYON_WIDTH
#define CANYON_WIDTH 20
#endif // CANYON_WIDTH

// game rate at 50Hz
#define GAME_FREQ 100000.0/FREQUENCY

#define DEBUG 0

#include <stdio.h>
 
typedef struct GameState {
  // offset from origin of each canyon step
  int * canyon_y_offsets;
  // index in the canyon offset array of 
  // the canyon at the bottom of the screen
  int canyon_index;
  // cycle y pos
  int cyclist_y;
  // default canyon position
  int default_canyon_y;
  // boolean to know if play lost (used by draw game)
  int lost;
  int x_max, y_max;
  int score;
  FILE * debug_log;
} GameState;

// create a GameState object
GameState init_game (int x, int y);

// run one tick of the game
int game_logic (GameState * game);

// draw the object on the screen
int draw_game (GameState * game);

// play one game and return score
int play_game ();

// load/free sprites
int load_game_sprites ();
int free_game_sprites ();

// callback on bike movement
void bike_left (GameState * game);
void bike_right (GameState * game);

#endif // GAME_H