#ifndef TXT_SPRITE_H
#define TXT_SPRITE_H

#define MAX_SPRITE_WIDTH 300
#define MAX_SPRITE_HEIGTH 300

typedef struct TxtSprite {
  unsigned int heigth;
  unsigned int max_width;
  char **text;
} TxtSprite;

// load a text psrite
TxtSprite load_txt_sprite (char *path);

// free text sprite
void free_txt_sprite (TxtSprite *sprite);

// display the sprint content
int print_sprite (TxtSprite sprite, int x, int y);
int printf_sprite (TxtSprite sprite);

#endif // TXT_SPRITE_H