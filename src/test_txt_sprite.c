#include <stdio.h>

#include "txt_sprite.h"

int main (int argc, char *argv[]) {
  printf ("Test sprite called\n");

  TxtSprite sprite = load_txt_sprite ("res/ascii/title.ascii");
  if (sprite.heigth == 0) {
    fprintf (stderr, "Error while loading image!\n");
    return 1;
  }

  printf_sprite (sprite);

  free_txt_sprite (&sprite);

  return 0;
}