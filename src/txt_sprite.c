#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "txt_sprite.h"

// load a text psrite
TxtSprite load_txt_sprite (char *path) {

  FILE *f;
  TxtSprite sprite;

  // opening the file
  f = fopen (path, "r");
  if (f == (FILE *)NULL) {
    sprite.heigth = 0;
    return sprite;
  }

  sprite.text = malloc (sizeof (char) * MAX_SPRITE_WIDTH * MAX_SPRITE_HEIGTH);

  if (sprite.text == NULL) {
    sprite.heigth = 0;
    return sprite;
  }

  sprite.max_width = 0;
  int width_buf;
  int iter = 0;
  char * current_line;

  // we read line per line
  char buf[MAX_SPRITE_WIDTH];
  while (fgets (buf, sizeof buf, f) != NULL) {
    // ignore the line if it's empty
    if (strcmp (buf, "") != 0) {
      current_line = (char *)sprite.text + (iter * MAX_SPRITE_WIDTH);
      strcpy (current_line, buf);
      current_line[strcspn(current_line, "\n")] = '\0';
      width_buf = strlen (current_line);
      if (width_buf > sprite.max_width)
        sprite.max_width = width_buf;
      iter++;
    }
  }

  sprite.heigth = iter;

  return sprite;
}

// free text sprite
void free_txt_sprite (TxtSprite *sprite) {
  free (sprite->text);
  return;
}

// display the sprint content
int print_sprite (TxtSprite sprite, int x, int y) {

  for (int i = 0; i < sprite.heigth; i++) {
    move (x+i, y);
    printw ("%s", (char*) sprite.text + (MAX_SPRITE_WIDTH * i));
  }

  return 0;
}

int printf_sprite (TxtSprite sprite) {
  for (int i = 0; i < sprite.heigth; i++) {
    printf ("%s\n", (char*) sprite.text + (i * MAX_SPRITE_WIDTH));
  }
  printf ("\n");
  return 0;
}