#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <string.h>

#include "game.h"
#include "menu.h"
#include "txt_sprite.h"
#include "scoreboard.h"

// define a macro to handle ctrl plus keys
#define ctrl(x) ((x)&0x1f)

// menu index
int menu_index = 0;
// menu page
unsigned int menu_page = 0;

// last refreshed time
struct timeval last_refreshed;
struct timeval time_buffer;

// menu elements coordinates
int cycle_y, def_cycle_y;

// counter to give cycle horizontal jitter
int last_moved_cycled;
int cycle_jitter_count;

// tree timeout and coordinates
int tree_y;
int tree_move_timer;

// last player name
char player_name[10];

// score board object
Scoreboard scoreboard;

// menu title sprite
TxtSprite menu_sprite, cloud_sprite_1, cloud_sprite_2, cycle_side_sprite,
    tree_sprite, rocks_sprite, arrow_sprite, small_menu_sprite,
    scoreboard_sprite;

void launch_menu (int is_sound) {

  // load scoreboard
  scoreboard = load_scoreboard();

#ifndef NO_SDL
  // init sound library
  SDL_Init (SDL_INIT_AUDIO);

  // load WAV file for the menu
  SDL_AudioSpec wavSpec, wavSpec2;
  Uint32 wavLength, wavLength2;
  Uint8 *wavBuffer, *wavBuffer2;
  if (SDL_LoadWAV ("res/music/TheCliffhanger.wav", &wavSpec, &wavBuffer,
                   &wavLength) == NULL) {
    fprintf (stderr, "Could not open audio ressource! Make sure to launch from "
                     "proper folder.\n");
    return;
  }
  if (SDL_LoadWAV ("res/music/02.wav", &wavSpec2, &wavBuffer2, &wavLength2) ==
      NULL) {
    fprintf (stderr, "Could not open audio ressource! Make sure to launch from "
                     "proper folder.\n");
    return;
  }

  // open audio device
  SDL_AudioDeviceID deviceId;

  // play music if sound was not deactivated
  if (is_sound == TRUE) {
    deviceId = SDL_OpenAudioDevice (NULL, 0, &wavSpec, NULL, 0);
    SDL_QueueAudio (deviceId, wavBuffer, wavLength);
    SDL_PauseAudioDevice (deviceId, 0);
  }
#endif // NO_SDL

  // load all sprites
  int error_sprite = load_sprites ();

  if (error_sprite == 1) {
    return;
  }

  // set cycle default position
  cycle_y = DEFAULT_CYCLE_Y;
  def_cycle_y = DEFAULT_CYCLE_Y;
  last_moved_cycled = 0;
  cycle_jitter_count = 0;
  // set tree display variables
  tree_y = MIN_WIN_SIZE / 2;
  tree_move_timer = 0;

  // init the timer used to refresh the screen
  gettimeofday (&last_refreshed, NULL);

  // cheating to get a first refreshh
  last_refreshed.tv_usec -= REFRESH_TIMEOUT;

  // init ncurses
  initscr ();

  // desactivate line buffering
  raw ();
  // disable write back when user type
  noecho ();
  // active special keys like arrows
  keypad (stdscr, TRUE);
  // activate non blocking getch
  nodelay (stdscr, TRUE);
  // hide cursor
  curs_set (0);

  // bool to tell main loop to stop
  int must_stop = FALSE;
  int error = 0;

  while (must_stop == FALSE) {
    // draw the main menu
    error = draw_main_menu (stdscr);

    // if there was an error, exit
    if (error != 0) {
      must_stop = TRUE;
      continue;
    }

    // get char
    int car = getch ();

    // stop if we hit escape or usual exit signals
    if (car == 27 || car == ctrl ('c') || car == ctrl ('d')) {
      must_stop = TRUE;
      continue;
    }

    if (car == KEY_DOWN) {
      menu_index++;
      if (menu_index > 2) {
        menu_index = 0;
      }
    }

    if (car == KEY_UP) {
      menu_index--;
      if (menu_index < 0) {
        menu_index = 2;
      }
    }

    // if enter is pressed, trigger the menu click callback
    if (car == 10) {
      // if quit is selected, quit the game
      if (menu_index == 2) {
        must_stop = TRUE;
        continue;
        // if scoreboard is selected
      } else if (menu_index == 1) {
        display_scoreboard (scoreboard, scoreboard_sprite);
        // if play is selected
      } else if (menu_index == 0) {
#ifndef NO_SDL
        if (is_sound == TRUE) {
          // pause the music
          SDL_ClearQueuedAudio (deviceId);
          SDL_PauseAudioDevice (deviceId, 1);
          SDL_CloseAudioDevice (deviceId);
          // play the main game music
          deviceId = SDL_OpenAudioDevice (NULL, 0, &wavSpec2, NULL, 0);
          SDL_QueueAudio (deviceId, wavBuffer2, wavLength2);
          SDL_PauseAudioDevice (deviceId, 0);
        }
#endif // NO_SDL
        // then launch the game
        int score = play_game ();
        // if there was an error, abort
        if (score == -1) {
          must_stop = TRUE;
          error = 1;
          continue;
        }
        // if score is greater than zero (0 <=> player quits)
        if (score > 0) {
          draw_death_screen (score);
          register_score (scoreboard, player_name, score);
          display_scoreboard (scoreboard, scoreboard_sprite);
        }
        // cheating to get a refresh
        last_refreshed.tv_usec -= REFRESH_TIMEOUT;
#ifndef NO_SDL
        // if sound is on
        if (is_sound == true) {
          // pause the game music
          SDL_ClearQueuedAudio (deviceId);
          SDL_PauseAudioDevice (deviceId, 1);
          SDL_CloseAudioDevice (deviceId);
          // play the menu music
          deviceId = SDL_OpenAudioDevice (NULL, 0, &wavSpec, NULL, 0);
          SDL_QueueAudio (deviceId, wavBuffer, wavLength);
          SDL_PauseAudioDevice (deviceId, 0);
        }
#endif // NO_SDL
      }
    }
  }

  // end
  endwin ();

  char *error_msg = NULL;

  // if there was an error, display message
  if (error != 0) {
    printf ("Erreur %d: ", error);
    get_error_message (error, &error_msg);
    printf ("%s\n", error_msg);
    must_stop = TRUE;
    free (error_msg);
  }

#ifndef NO_SDL
  // clean up for the sdl library
  SDL_CloseAudioDevice (deviceId);
  SDL_FreeWAV (wavBuffer);
  SDL_Quit ();
#endif // NO_SDL

  return;
}

// draws the main menu
int draw_main_menu (WINDOW *win) {

  gettimeofday (&time_buffer, NULL);

  if (labs (time_buffer.tv_usec - last_refreshed.tv_usec) < REFRESH_TIMEOUT) {
    return 0;
  }

  // get window heigth/width
  int x, y;
  getmaxyx (win, x, y);

  if (x < MIN_WIN_SIZE || y < MIN_WIN_SIZE * 3) {
    // if width is too small, abort and print and error
    fprintf (stderr, "wrong screen size: x:%d y:%d\n", x, y);
    return 1;
  }

  // clear the screen
  erase ();

  // display title if screen is width enough
  if (y > menu_sprite.max_width)
    print_sprite (menu_sprite, 0, (y - menu_sprite.max_width) / 2);
  else if (y > small_menu_sprite.max_width)
    print_sprite (small_menu_sprite, 0, (y - small_menu_sprite.max_width) / 2);
  else {
    move (4, (y - strlen ("Cyclic Death")) / 2);
    printw ("Cyclic Death");
  }

  // move cycle every CYCLE_JITTER periods
  if (cycle_jitter_count > CYCLE_JITTER) {
    cycle_jitter_count = 0;
    // move cycle forward 4 times before restarting
    cycle_y = (cycle_y + 1);
    if (cycle_y - def_cycle_y > 3)
      cycle_y = def_cycle_y;
  } else {
    cycle_jitter_count++;
  }
  // display cycle
  print_sprite (cycle_side_sprite, x - cycle_side_sprite.heigth, cycle_y);

  // move tree
  if (tree_move_timer > TREE_MOVE_TIME) {
    tree_move_timer = 0;
    tree_y = tree_y - 1;
    if (tree_y < 0) {
      tree_y = y - tree_sprite.max_width;
    }
  } else {
    tree_move_timer++;
  }
  // display tree
  print_sprite (tree_sprite, x - tree_sprite.heigth, tree_y);

  // display menu texts
  // there is currently 3 options, so we'll approximately need a 6x13 rectangle
  // let's compute the top anchor y coordinates
  // given that tree_sprite is the highest sprite at bottom
  int anchor_menu_top =
      menu_sprite.heigth +
      (((x - menu_sprite.heigth) - tree_sprite.heigth) - 6) / 2;
  int anchor_menu_left = (y - 13) / 2;

  // display menu items
  move (anchor_menu_top, anchor_menu_left);
  printw ("1) PLAY");
  move (anchor_menu_top + 2, anchor_menu_left);
  printw ("2) SCOREBOARD");
  move (anchor_menu_top + 4, anchor_menu_left);
  printw ("3) QUIT");

  // display the ascii cursor pointing at the current menu
  int arrow_y = anchor_menu_left - arrow_sprite.max_width - 3;
  int arrow_x = anchor_menu_top + (menu_index * 2);
  print_sprite (arrow_sprite, arrow_x, arrow_y);

  // moving the ncurses cursor to the end
  move (x - 1, y - 1);
  printw (" ");

  last_refreshed = time_buffer;

  // display what was loaded
  refresh ();

  return 0;
}

void get_error_message (int error_nb, char **error_msg) {
  // allocate space for error message
  *error_msg = malloc (sizeof (char) * 50);

  if (error_nb == 1) {
    strcpy (*error_msg, "Mauvaise taille ecran, aggrandissez la fenêtre.");
    return;
  } else {
    strcpy (*error_msg, "Erreur inconnue");
    return;
  }
}

int load_sprites () {
  // load image for the title
  menu_sprite = load_txt_sprite ("res/ascii/title.ascii");
  if (menu_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  small_menu_sprite = load_txt_sprite ("res/ascii/title_small.ascii");
  if (small_menu_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  // load image for the clouds
  cloud_sprite_1 = load_txt_sprite ("res/ascii/clouds.ascii");
  if (cloud_sprite_1.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  // load image for the clouds
  cloud_sprite_2 = load_txt_sprite ("res/ascii/clouds2.ascii");
  if (cloud_sprite_2.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  cycle_side_sprite = load_txt_sprite ("res/ascii/cycle_side.ascii");
  if (cycle_side_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  tree_sprite = load_txt_sprite ("res/ascii/tree.ascii");
  if (tree_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  rocks_sprite = load_txt_sprite ("res/ascii/rock.ascii");
  if (rocks_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  arrow_sprite = load_txt_sprite ("res/ascii/arrow.ascii");
  if (arrow_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  scoreboard_sprite = load_txt_sprite ("res/ascii/scoreboard.ascii");
  if (scoreboard_sprite.heigth == 0) {
    fprintf (stderr, "Could not open ascii ressource! Make sure to launch from "
                     "proper folder.\n");
    return 1;
  }

  return load_game_sprites ();
}

// free the sprites
int free_sprites () { return free_game_sprites (); }

// display score screen after player died, save name in player_name
void draw_death_screen (int score) {

  // initiates name length
  int name_len = 0;

  // clear the screen
  erase ();

  // get window heigth/width
  int x, y;
  getmaxyx (stdscr, x, y);

  if (x < MIN_WIN_SIZE || y < MIN_WIN_SIZE) {
    // if width is too small, abort and print and error
    fprintf (stderr, "wrong screen size: x:%d y:%d\n", x, y);
    return;
  }

  // display score
  move (x / 3 - 1, y / 2 - 10);
  printw ("Score: %d", score);

  // get player name
  move (2 * x / 3 - 1, y / 2 - 20);
  printw ("Your name:");

  int done = FALSE;

  // loop to get username
  while (done == FALSE) {
    // get pressed key
    int car = getch ();
    // if it's a letter and name string is not full
    if (((car > 'A' && car < 'Z') || (car > 'a' && car < 'z')) &&
        name_len < 10) {
      // print it and save it
      printw ("%c", car);
      player_name[name_len] = (char)car;
      name_len++;
      // refresh screen
      refresh ();
      // if enter is pressed while name string longer than 3 char
      // or name string is full
    } else if ((car == 10 && name_len > 3) || name_len >= 10) {
      // put the end of string char
      player_name[name_len] = '\0';
      // clear the screen
      clear ();
      // return
      return;
    }
  }

  return;
}