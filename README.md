# CyclicDeath
Programming assignment for the L3 Math & Computer Science classes.

## Description
Ncurse arcade game where you need to ride a bike through a moving cliff.

## Additional features
We were asked to add features of our choice, we implemented:
- Main menu (PLAY / SCOREBOARD / QUIT)
- Scoreboard
- Homemade game music (made with Linux Multi Media Studio)
- Animation at the bottom of the main menu, title resizement depending on terminal size
- Sprite library
- Continous Integration for tests that run in GitLab's cloud containers
- Clang automatic formatting

## Deploy
To build with sound and default parameters:
```bash
cd build
make
cd ..
./build/bin/cyclic_death
```

To build without the SDL library for sound:
```bash
cd build
make PARAMETERS="-DNO_SDL"
cd ..
./build/bin/cyclic_death
```

To build with custom parameters:
```bash
cd build
make PARAMETERS="-DFREQUENCY=10.0 -DCANYON_WIDTH=20"
cd ..
./build/bin/cyclic_death
```

## Compilation parameters
### FREQUENCY
A float number reprensenting the main game frequency in Hertz.

### CANYON_WIDTH
An integer definition the canyon width.

### NO_SDL
If defined, the SDL library will not be included and the sound will no be used.