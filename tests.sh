#!/bin/sh

echo -e "\033[33m========== \033[33m Execution des tests pour le paquetage \033[35m txt_sprite \033[33m ===========\033[0m"
printf "%-30.30s" "Execution des tests..."
./build/bin/test_txt_sprite > test/test_txt_sprite.output
echo -e "\033[32mReussite\033[0m"
printf "%-30.30s" "Verification des sorties..."
if diff test/test_txt_sprite.output test/test_txt_sprite.expected
then
	echo -e "\033[32mReussite\033[0m"
else
	echo -e "\033[31mEchec\033[0m"
	exit 1
fi

echo " "
echo -e "\033[32mTous les tests on été passés avec succès!\033[0m"
